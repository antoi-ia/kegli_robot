#include"timer-api.h"

#define BAT_TR            10
#define BAT_TR_OFF        15

#define SPEED_LEFT         5
#define SPEED_RIGHT        6
#define DIR_LEFT           4
#define DIR_RIGHT          7
#define START_BUTTON       2
#define START_LED          3 //Индикация старт

typedef enum
{
  L_FW = 1,
  L_BW = 0,
}DIR_L;

typedef enum
{
  R_FW = 0,
  R_BW = 1,
}DIR_R;

enum step_time_enum
{
  t0_time = 0,
  t1_time = 1,
  t2_time,
  t3_time,
};

typedef struct
{
  unsigned int Time;
  unsigned int SpeedL;
  unsigned int SpeedR;
  DIR_L DirL;
  DIR_R DirR;
} Timers;

struct TIM_struct
{
  enum step_time_enum step;
  unsigned int time_10ms;
};

typedef struct
{
  byte NO_BAT;
  byte START;
  byte START_F;
}bat_s;

// глобальные переменные 
int currentSpeedL = 0;
int currentSpeedR = 0;

DIR_L currentDirL = L_FW;
DIR_R currentDirR = R_FW;

boolean start;            // статус работы

Timers tim1,tim2,tim3;    // структуры таймеров
struct TIM_struct t;      // структура работы таймеров
bat_s bat;                // структура кнопок

void setup()
{

  // настроим таймеры
  tim1.Time = 200;
  tim1.SpeedL = 100;
  tim1.SpeedR = 100;
  tim1.DirL = L_FW;
  tim1.DirR = R_FW;

  tim2.Time = 500;
  tim2.SpeedL = 100;
  tim2.SpeedR = 100;
  tim2.DirL = L_FW;
  tim2.DirR = R_FW;

  tim3.Time = 700;
  tim3.SpeedL = 100;
  tim3.SpeedR = 100;
  tim3.DirL = L_FW;
  tim3.DirR = R_FW;

  t.step = t0_time;         // стартовый шаг таймера 0
  // таймеры настроены
  
  // стартовая настройка кнопки
  bat.START = BAT_TR;
  bat.NO_BAT = BAT_TR;
  // кнопки настроены

  // настройка таймера на 100 Гц
  timer_init_ISR_100Hz(TIMER_DEFAULT);
  
  // настройка Uart
  Serial.begin(9600);
  
  // настройка пинов
  pinMode(SPEED_LEFT,OUTPUT);
  pinMode(SPEED_RIGHT,OUTPUT);
  pinMode(DIR_LEFT,OUTPUT); 
  pinMode(DIR_RIGHT, OUTPUT);
  pinMode(START_BUTTON, INPUT);
  pinMode(START_LED, OUTPUT); 
}

/**
 * Процедура, вызываемая прерыванием по событию таймера с заданным периодом
 * @param timer - идентификатор таймера
 */
void timer_handle_interrupts(int timer) 
{
  t.time_10ms--;
  if (!t.time_10ms)
  {
    switch (t.step)
    {
        case t1_time:
        // тут мы окажемся после срабатывания времени по таймеру 1
        // параметры шага 2 тайера        
        t.time_10ms = tim2.Time;
        currentSpeedL = tim2.SpeedL;
        currentSpeedR = tim2.SpeedR;
        currentDirL = tim2.DirL;
        currentDirR = tim2.DirR;

        t.step = t2_time;
        
        Serial.println("Step T1 end");
      break;
      
        case t2_time:
        // параметры шага 3 тайера
        t.time_10ms = tim3.Time;
        currentSpeedL = tim3.SpeedL;
        currentSpeedR = tim3.SpeedR;
        currentDirL = tim3.DirL;
        currentDirR = tim3.DirR;

        t.step = t3_time;
        
        Serial.println("Step T2 end");
      break;
      
        case t3_time:
        currentSpeedL = 0;
        currentSpeedR = 0;
        
        t.step = t0_time;
        start = LOW;
        bat.START_F = 0;

        Serial.println("Step T3 end");
      break;
      
        default:
      break;
    }
  }
}

void loop() 
{
  // обработка кнопки
  if (digitalRead(START_BUTTON))
  {
    if (bat.START<BAT_TR)bat.START--;
    bat.NO_BAT = BAT_TR;
  }
  else
  {
    if (bat.START>(BAT_TR-2))bat.START--;
  }

  if (!bat.START)
  {
    bat.START_F=!bat.START_F;       // меняем флаг кнопки на противоположный
    bat.START = BAT_TR_OFF;         // ставим значение переменной
    
    // действие при нажатии
    if (bat.START_F)                // если флаг активен то запускаем шаг таймера
    {
      t.time_10ms = tim1.Time;
      t.step = t1_time;
      // параметры шага 1 тайера
      currentSpeedL = tim1.SpeedL;
      currentSpeedR = tim1.SpeedR;
      currentDirL = tim1.DirL;
      currentDirR = tim1.DirR;
      start = HIGH;
    }
    else                            // если флаг не активен то ставим шаг 0
    {
      start = LOW;
      t.step = t0_time;
    }

  }
  // конец обработки кнопки
  
  digitalWrite(START_LED, start);   // выведем на светодиод состояние переменной start
  if (start == HIGH) // если кнопку  нажали  
  {
    analogWrite(SPEED_LEFT, currentSpeedL);           // задание скорости
    analogWrite(SPEED_RIGHT, currentSpeedR);          // задание скорости
    digitalWrite(DIR_LEFT, currentDirL);              // направление 
    digitalWrite(DIR_RIGHT, currentDirR);             // направление 
  }
  else
  {
    analogWrite(SPEED_LEFT, 0);                // скорость на 0 стоим
    analogWrite(SPEED_RIGHT, 0);
    digitalWrite(DIR_LEFT, HIGH);              // направление вперед
    digitalWrite(DIR_RIGHT, HIGH);
  }
  delay(20);
}
